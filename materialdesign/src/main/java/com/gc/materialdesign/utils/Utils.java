package com.gc.materialdesign.utils;

import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * 工具类
 */
public class Utils {

    /**
     * dp转化为px
     * @param dp dp
     * @param context context
     * @return px
     */
    public static int dpToPx(float dp, Context context) {
        float scale = context.getResourceManager().getDeviceCapability().screenDensity;
        return (int) (dp * scale / 160 + 0.5f);
    }

    /**
     * 组件相对顶部的距离
     * @param myView 目标组件
     * @return 相对顶部的距离
     */
    public static int getRelativeTop(Component myView) {
        if (myView.getComponentParent() == null) {
            return myView.getTop();
        } else {
            return myView.getTop() + getRelativeTop((Component) myView.getComponentParent());
        }
    }

    /**
     * 组件相对左边的距离
     * @param myView 目标组件
     * @return 相对左边的距离
     */
    public static int getRelativeLeft(Component myView) {
        if (myView.getComponentParent() == null) {
            return myView.getLeft();
        } else {
            return myView.getLeft() + getRelativeLeft((Component) myView.getComponentParent());
        }
    }

    /**
     * 颜色转化rgba
     * @param colorStr 颜色
     * @return rgba
     */
    public static String rgba(String colorStr) {
        return colorStr.length() == 7 ? colorStr + "FF" : colorStr;
    }

    /**
     * 颜色转化argb
     * @param colorStr 颜色
     * @return argb
     */
    public static String argb(String colorStr) {
        return colorStr.length() == 7
                ? "#FF" + colorStr.replace("#", "")
                : "#"
                        + colorStr.substring(colorStr.length() - 2)
                        + colorStr.substring(0, colorStr.length() - 2).replace("#", "");
    }

    /**
     * 颜色转化argb
     * @param colorStr 颜色
     * @param alpha 透明度
     * @return argb
     */
    public static String argb(String colorStr, String alpha) {
        return colorStr.length() == 7
                ? "#" + alpha + colorStr.replace("#", "")
                : "#" + alpha + colorStr.substring(0, colorStr.length() - 2).replace("#", "");
    }
}
