/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gc.materialdesign.utils;

import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

/**
 * Bitmap 压缩相关操作工具类
 */
public class ImageUtil {
    /**
     * 从Resources中加载图片
     * @param context 上下文
     * @param resId 资源id
     * @return Bitmap
     */
    public static PixelMap decodeSampledBitmapFromResource(Context context, int resId) {
        String path = FileUtil.getPathById(context, resId);
        if (StringUtil.isEmpty(path)) {
            return null;
        }
        ImageSource imageSource;
        RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/png";
        try {
            Resource asset = assetManager.openRawFile();
            imageSource = ImageSource.create(asset, options);
        } catch (IOException e) {
            return null;
        }
        if (imageSource == null) {
            return null;
        }
        PixelMap pixelMap = imageSource.createPixelmap(null);
        return pixelMap;
    }
}
