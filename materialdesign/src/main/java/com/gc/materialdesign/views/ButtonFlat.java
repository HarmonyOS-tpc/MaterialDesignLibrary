package com.gc.materialdesign.views;

import com.gc.materialdesign.utils.Utils;

import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class ButtonFlat extends Button {
    private Text textButton;

    public ButtonFlat(Context context) {
        this(context, null);
    }

    public ButtonFlat(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ButtonFlat(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void setDefaultProperties() {
        minHeight = 36;
        minWidth = 88;
        rippleSize = 3;
        // Min size
        setMinHeight(Utils.dpToPx(minHeight, context));
        setMinWidth(Utils.dpToPx(minWidth, context));
    }

    @Override
    protected void setAttributes(AttrSet attrs) {
        // Set text button
        boolean textIsPresent = attrs.getAttr("text").isPresent();
        String text = null;
        if (textIsPresent) {
            text = attrs.getAttr("text").get().getStringValue();
        }
        if (text != null) {
            textButton = new Text(context);
            textButton.setText(text.toUpperCase());
            textButton.setTextColor(new Color(Color.getIntColor(Utils.argb(backgroundColor))));
            textButton.setFont(Font.DEFAULT_BOLD);
            textButton.setTextSize(14, Text.TextSizeType.FP);
            LayoutConfig layoutConfig =
                    new DependentLayout.LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
            layoutConfig.addRule(LayoutConfig.CENTER_IN_PARENT, LayoutConfig.TRUE);
            textButton.setLayoutConfig(layoutConfig);
            addComponent(textButton);
        }

        boolean backgroundIsPresent = attrs.getAttr("background").isPresent();
        if (backgroundIsPresent) {
            String color = attrs.getAttr("background").get().getStringValue();
            setBackgroundColor(color);
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        super.onDraw(component, canvas);
        if (x != -1) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(new Color(Color.getIntColor(makePressColorString())));
            canvas.drawCircle(x, y, radius, paint);
            if (radius > getHeight() / rippleSize) {
                radius += rippleSpeed;
            }
            if (radius >= getWidth()) {
                x = -1;
                y = -1;
                radius = getHeight() / rippleSize;
                if (onClickListener != null && clickAfterRipple) {
                    onClickListener.onClick(this);
                }
            }
            getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        }
    }

    /**
     * Make a dark color to ripple effect
     *
     * @return 颜色值
     */
    @Override
    protected String makePressColorString() {
        return "#10666666";
    }

    public void setText(String text) {
        textButton.setText(text.toUpperCase());
    }

    // Set color of background
    public void setBackgroundColor(String color) {
        backgroundColor = color;
        if (isEnabled()) {
            beforeBackground = backgroundColor;
        }
        textButton.setTextColor(new Color(Color.getIntColor(Utils.argb(color))));
    }

    @Override
    public Text getTextView() {
        return textButton;
    }

    public String getText() {
        return textButton.getText();
    }
}
