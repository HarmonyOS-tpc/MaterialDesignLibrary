package com.gc.materialdesign.views;

import com.gc.materialdesign.ResourceTable;
import com.gc.materialdesign.utils.ShapeUtil;
import com.gc.materialdesign.utils.Utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class ProgressBarDeterminate extends CustomView {
    protected boolean firstProgress = true;
    private int max = 100;
    private int min = 0;
    private int progress = 0;

    String backgroundColor = "#1E88E5FF";
    private Context context;

    protected Component progressView;

    public ProgressBarDeterminate(Context context) {
        this(context, null);
    }

    public ProgressBarDeterminate(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ProgressBarDeterminate(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setAttributes(attrs);
    }

    // Set atributtes of XML to View
    protected void setAttributes(AttrSet attrs) {
        progressView = new Component(context);
        LayoutConfig params = new DependentLayout.LayoutConfig(1, 1);
        progressView.setLayoutConfig(params);
        progressView.setBackground(new ShapeElement(context, ResourceTable.Graphic_background_progress));
        addComponent(progressView);

        // Set background Color
        // Color by resource
        boolean backgroundIsPresent = attrs.getAttr("background").isPresent();
        if (backgroundIsPresent) {
            String color = attrs.getAttr("background").get().getStringValue();
            setBackgroundColor(color);
        }

        boolean minIsPresent = attrs.getAttr("min").isPresent();
        if (minIsPresent) {
            min = attrs.getAttr("min").get().getIntegerValue();
        }

        boolean maxIsPresent = attrs.getAttr("max").isPresent();
        if (maxIsPresent) {
            max = attrs.getAttr("max").get().getIntegerValue();
        }

        boolean progressIsPresent = attrs.getAttr("progress").isPresent();
        if (progressIsPresent) {
            progress = attrs.getAttr("progress").get().getIntegerValue();
        } else {
            progress = min;
        }
        setProgress(progress);

        setMinHeight(Utils.dpToPx(3, context));

        getComponentTreeObserver().addWindowBoundListener(new ComponentTreeObserver.WindowBoundListener() {
            @Override
            public void onWindowBound() {
                LayoutConfig layoutConfig = (LayoutConfig) progressView.getLayoutConfig();
                layoutConfig.height = getHeight();
                progressView.setLayoutConfig(layoutConfig);
            }

            @Override
            public void onWindowUnbound() {

            }
        });

    }

    /**
     * Make a dark color to ripple effect
     *
     * @return 改变后的颜色值
     */
    protected String makePressColorString() {
        int colorInt;
        if (backgroundColor.length() == 7) {
            colorInt = Color.getIntColor(backgroundColor);
        } else {
            colorInt = Color.getIntColor(backgroundColor.substring(0, backgroundColor.length() - 2));
        }
        int r = (colorInt >> 16) & 0xFF;
        int g = (colorInt >> 8) & 0xFF;
        int b = (colorInt >> 0) & 0xFF;
        StringBuilder builder =
                new StringBuilder("#")
                        .append(colorPrefix(Integer.toHexString(r)))
                        .append(colorPrefix(Integer.toHexString(g)))
                        .append(colorPrefix(Integer.toHexString(b)))
                        .append(Integer.toHexString(128));
        return builder.toString();
    }

    private String colorPrefix(String string) {
        return string.length() > 1 ? string : "00";
    }

    // SETTERS
    @Override
    public void onDraw(Component component, Canvas canvas) {
        super.onDraw(component, canvas);
        if (pendindProgress != -1) {
            setProgress(pendindProgress);
        }
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setMin(int min) {
        this.min = min;
    }

    private int pendindProgress = -1;

    public void setProgress(int progress) {
        if (getWidth() == 0) {
            pendindProgress = progress;
        } else {
            this.progress = progress;
            if (progress > max) {
                progress = max;
            }
            if (progress < min) {
                progress = min;
            }
            int totalWidth = max - min;
            double progressPercent = (double) progress / (double) totalWidth;
            int progressWidth = (int) (getWidth() * progressPercent);
            LayoutConfig params = (LayoutConfig) progressView.getLayoutConfig();
            params.width = progressWidth;
            params.height = getHeight();
            progressView.setLayoutConfig(params);
            pendindProgress = -1;
        }
    }

    public int getProgress() {
        return progress;
    }

    // Set color of background
    public void setBackgroundColor(String rgba) {
        this.backgroundColor = rgba;
        if (isEnabled()) {
            beforeBackground = backgroundColor;
        }
        setBackground(ShapeUtil.createDrawable(Color.getIntColor(makePressColorString())));
        progressView.setBackground(
                ShapeUtil.createDrawable(
                        Color.getIntColor(Utils.rgba(rgba)),
                        0,
                        Utils.dpToPx(100, context),
                        Utils.dpToPx(100, context),
                        0));
    }
}
