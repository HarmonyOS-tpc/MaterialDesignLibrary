package com.gc.materialdesign.views;

import com.gc.materialdesign.ResourceTable;
import com.gc.materialdesign.utils.ShapeUtil;
import com.gc.materialdesign.utils.Utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class ButtonRectangle extends Button {
    Text textButton;

    public ButtonRectangle(Context context) {
        this(context, null);
    }

    public ButtonRectangle(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ButtonRectangle(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setDefaultProperties();
    }

    @Override
    protected void setDefaultProperties() {
        super.minWidth = 80;
        super.minHeight = 50;
        super.background = ResourceTable.Graphic_background_button_rectangle;
        super.setDefaultProperties();
    }

    // Set atributtes of XML to View
    protected void setAttributes(AttrSet attrs) {
        // Set background Color
        // Color by resource
        boolean backgroundIsPresent = attrs.getAttr("background").isPresent();
        if (backgroundIsPresent) {
            String color = attrs.getAttr("background").get().getStringValue();
            setBackgroundColor(color);
            setBackground(ShapeUtil.createDrawable(Color.getIntColor(Utils.rgba(color)), 2));
        } else {
            setBackground(ShapeUtil.createDrawable(Color.getIntColor(backgroundColor), 2));
            setBackgroundColor(backgroundColor);
        }

        // Set text button
        String text = null;
        boolean textIsPresent = attrs.getAttr("text").isPresent();
        if (textIsPresent) {
            text = attrs.getAttr("text").get().getStringValue();
        }
        if (text != null) {
            textButton = new Text(getContext());
            textButton.setText(text);
            textButton.setTextColor(Color.WHITE);
            textButton.setFont(Font.DEFAULT_BOLD);
            textButton.setTextSize(14, Text.TextSizeType.FP);
            LayoutConfig params = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
            params.addRule(LayoutConfig.CENTER_IN_PARENT, LayoutConfig.TRUE);
            params.setMargins(
                    Utils.dpToPx(5, context),
                    Utils.dpToPx(5, context),
                    Utils.dpToPx(5, context),
                    Utils.dpToPx(5, context));
            textButton.setLayoutConfig(params);
            addComponent(textButton);

            boolean textColorIsPresent = attrs.getAttr("textColor").isPresent();
            if (textColorIsPresent) {
                String textColor = attrs.getAttr("textColor").get().getStringValue();
                textButton.setTextColor(new Color(Color.getIntColor(Utils.argb(textColor))));
            }

            boolean textSizeIsPresent = attrs.getAttr("textSize").isPresent();
            if (textSizeIsPresent) {
                int textSize = attrs.getAttr("textSize").get().getDimensionValue();
                textButton.setTextSize(textSize);
            }
        }

        boolean rippleSpeedIsPresent = attrs.getAttr("rippleSpeed").isPresent();
        if (rippleSpeedIsPresent) {
            rippleSpeed = attrs.getAttr("rippleSpeed").get().getFloatValue();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        super.onDraw(component, canvas);
        if (x != -1) {
            RectFloat src =
                    new RectFloat(0, 0, getWidth() - Utils.dpToPx(6, context), getHeight() - Utils.dpToPx(7, context));
            RectFloat dst = new RectFloat(0, 0, getWidth(), getHeight());
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            canvas.drawPixelMapHolderRect(new PixelMapHolder(makeCircle()), src, dst, paint);
            getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        }
    }
}
