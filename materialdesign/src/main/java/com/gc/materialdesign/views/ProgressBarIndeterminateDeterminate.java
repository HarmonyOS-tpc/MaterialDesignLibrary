package com.gc.materialdesign.views;

import com.gc.materialdesign.utils.ViewHelper;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.app.Context;

public class ProgressBarIndeterminateDeterminate extends ProgressBarDeterminate {
    private boolean runAnimation = true;
    private AnimatorProperty animation;

    public ProgressBarIndeterminateDeterminate(Context context) {
        this(context, null);
    }

    public ProgressBarIndeterminateDeterminate(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ProgressBarIndeterminateDeterminate(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // Make progress animation
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        firstProgress = true;
        setProgress(60);
        ViewHelper.setX(progressView, getWidth() + progressView.getWidth() / 2);
        animation = new AnimatorProperty(progressView);
        animation.moveFromX(0).moveToX(progressView.getWidth() / 2);
        animation.setDuration(1200);
        animation.setStateChangedListener(listener);
        animation.start();
        super.onDraw(component, canvas);
    }

    private Animator.StateChangedListener listener =
            new Animator.StateChangedListener() {
                int cont = 1;
                int suma = 1;
                int duration = 1200;

                @Override
                public void onStart(Animator animator) {}

                @Override
                public void onStop(Animator animator) {}

                @Override
                public void onCancel(Animator animator) {}

                @Override
                public void onEnd(Animator animator) {
                    // Repeat animation
                    if (runAnimation) {
                        ViewHelper.setX(progressView, getWidth() + progressView.getWidth() / 2);
                        cont += suma;
                        animation = new AnimatorProperty(progressView);
                        animation.moveFromX(0).moveToX(progressView.getWidth() / 2);
                        animation.setDuration(duration / cont);
                        animation.setStateChangedListener(this);
                        animation.start();
                        if (cont == 3 || cont == 1) {
                            suma *= -1;
                        }
                    }
                }

                @Override
                public void onPause(Animator animator) {}

                @Override
                public void onResume(Animator animator) {}
            };

    @Override
    public void setProgress(int progress) {
        if (firstProgress) {
            firstProgress = false;
        } else {
            stopIndeterminate();
        }
        super.setProgress(progress);
    }

    /**
     * Stop indeterminate animation to convert view in determinate progress bar
     */
    private void stopIndeterminate() {
        animation.cancel();
        ViewHelper.setX(progressView, 0);
        runAnimation = false;
    }
}
