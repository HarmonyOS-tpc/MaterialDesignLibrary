package com.gc.materialdesign.views;

import com.gc.materialdesign.ResourceTable;
import com.gc.materialdesign.utils.ShapeUtil;
import com.gc.materialdesign.utils.Utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class ButtonFloatSmall extends ButtonFloat {
    public ButtonFloatSmall(Context context) {
        this(context, null);
    }

    public ButtonFloatSmall(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ButtonFloatSmall(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        sizeRadius = 20;
        sizeIcon = 20;
        setDefaultProperties();
    }

    protected void setDefaultProperties() {
        rippleSpeed = Utils.dpToPx(2, context);
        rippleSize = 10;
        // Min size
        setMinHeight(Utils.dpToPx(sizeRadius * 2, context));
        setMinWidth(Utils.dpToPx(sizeRadius * 2, context));
        // Background shape
        ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_background_button_float);
        setBackground(shapeElement);
    }

    // Set color of background
    public void setBackgroundColor(String color) {
        super.setBackgroundColor(color);
        setBackground(ShapeUtil.createCircleDrawable(Color.getIntColor(Utils.rgba(color))));
    }
}
