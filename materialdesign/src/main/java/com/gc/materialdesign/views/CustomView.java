package com.gc.materialdesign.views;

import com.gc.materialdesign.utils.Utils;

import ohos.agp.animation.Animator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class CustomView extends DependentLayout implements Component.DrawTask, Animator.StateChangedListener {
    final int disabledBackgroundColor = Color.getIntColor("#FFE2E2E2");
    String beforeBackground;

    // Indicate if user touched this view the last time
    public boolean isLastTouch = false;

    public CustomView(Context context) {
        this(context, null);
    }

    public CustomView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public CustomView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addDrawTask(this);
        createAnimatorProperty().setStateChangedListener(this);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        ShapeElement shapeElement = new ShapeElement();
        if (enabled) {
            shapeElement.setRgbColor(new RgbColor(Color.getIntColor(Utils.rgba(beforeBackground))));
        } else {
            shapeElement.setRgbColor(new RgbColor(disabledBackgroundColor));
        }
        setBackground(shapeElement);
        invalidate();
    }

    boolean animation = false;

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (animation) {
            getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        }
    }

    @Override
    public void onStart(Animator animator) {
        animation = true;
    }

    @Override
    public void onStop(Animator animator) {
        animation = false;
    }

    @Override
    public void onCancel(Animator animator) {}

    @Override
    public void onEnd(Animator animator) {}

    @Override
    public void onPause(Animator animator) {}

    @Override
    public void onResume(Animator animator) {}
}
